/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.on('/').render('welcome')

Route.on('admin/login').render('auth/login')
Route.post('login', 'Auth/AuthController.login')
Route.group(() => {
  Route.get('/dashboard', async ({ view,auth }) => {
    const user = await auth.use('admin').authenticate()
    return view.render('admin/dashboard',{user})
  }) 
  Route.get('logout','Auth/AuthController.logout').as('logout')
}).middleware('auth:admin')

// api routes
Route.post('/api/login', 'Auth/Api/AuthController.login')
Route.group(() => {
  Route.get('/api/patners','PatnersController.index')
  Route.get('api/dashboard', async ({ auth }) => {
    const user = await auth.use('api').authenticate() //  👈 All you need to go
    return user.toJSON()
  })
  Route.get('api/logout','Auth/Api/AuthController.logout')
}).middleware('auth:api')

