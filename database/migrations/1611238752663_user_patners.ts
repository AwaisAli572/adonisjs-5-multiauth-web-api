import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UserPatners extends BaseSchema {
  protected tableName = 'user_patners'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.bigInteger('user_id').notNullable()
      table.bigInteger('patner_id').notNullable()
      table.string('points').notNullable()
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
