import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Patners extends BaseSchema {
  protected tableName = 'patners'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name').notNullable()
      table.string('logo').notNullable()
      table.string('buying_rate').notNullable()
      table.string('selling_rate').notNullable()
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
