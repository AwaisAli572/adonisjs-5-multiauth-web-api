import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Admin from 'App/Models/Admin'
// const bcrypt = require('bcrypt');
import Hash from '@ioc:Adonis/Core/Hash'

export default class AdminSeeder extends BaseSeeder {
  public async run () {
    // Write your database queries inside the run method
    await Admin.create({
      name: 'Awais',
      email:'admin@site.com',
      // password: bcrypt.hashSync('admin123', 10)
      password: await Hash.make('admin123')
    }
    )
  }
}
