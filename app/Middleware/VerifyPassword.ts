import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import Hash from '@ioc:Adonis/Core/Hash'

export default class VerifyPassword {
  public async handle ({ request, auth, response}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    const { email, password } = request.all();
    const user = await User.findBy('email', email)
    const passwControl = await Hash.verify(password, user.password)

    if (!passwControl) {
      return response.status(400).json({
        message: "incorrect password!"
      });
    }
    await next()
  }
}
