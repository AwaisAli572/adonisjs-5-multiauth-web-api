import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class AuthController {
    public async login ({ auth, request, response }: HttpContextContract) {
        const email = request.input('email')
        const password = request.input('password')
        await auth.use('admin').attempt(email, password)
        response.redirect('/dashboard')
      }
      public async logout({auth,response}){
          await auth.use('admin').logout()
          response.redirect('login')
      }
}
