import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class AuthController {
    public async login ({ auth, request }: HttpContextContract) {
        const email = request.input('email')
        const password = request.input('password')
        const token = await auth.use('api').attempt(email, password)
        return token.toJSON()
    }
    public async logout({auth}){
        await auth.use('api').logout()
        return {
            'code':200,
            'message':'logout success'
        }
    }
}
