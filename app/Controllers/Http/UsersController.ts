import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules, validator } from '@ioc:Adonis/Core/Validator'
import Patner from 'App/Models/Patner'
import UserPatner from 'App/Models/UserPatner'

export default class UsersController {
    public async index ({view}: HttpContextContract) {
        const patners = await Patner.all()
        return view.render('admin/patner/index',{patners})
      }
      public async addPatner ({auth, request}: HttpContextContract) {
        /**
         * Validate user details
         */
        const validationSchema = schema.create({
          patner_id: schema.number(),
          points: schema.string({ trim: true }),
        })
    
        const data = await request.validate({
          schema: validationSchema,
          messages:{
              'patner_id.required':'Patner id is required',
              'points.required':'Points are required'
          },
          reporter: validator.reporters.api, // 👈 using reporter
        })
    
        /**
         * Create a new patner
         */
        const userId   = await auth.use('api').authenticate()
        const user     = new UserPatner()
        user.user_id   = userId.id
        user.patner_id = data.patner_id
        user.points    = data.points
        await user.save()
    
        return {
            'code':201,
            'message':'Patner added success'
        }
      }
}
